% Preproc_4_4_SvM
% This part of the script was done on personal computer because it was
% easier and didn't require changing the contents of the main.m script
% Also download Hori stage information excels (Valdas folder) for this

% Define important paths
clear all
close all
datafolder = 'C:\Datafiles_cam'; % Download MEP and EEG length files to this folder;

%% check trial numbers again - OK

cd(datafolder);

MATRIX = zeros(20,2);

for sub = [2:21]
    load(['trial_length_2_sub' num2str(sub) '.mat']);
    MATRIX(sub-1,1) = trialN_4_3;

    Hori = xlsread(['QBI_Hori_' num2str(sub)]);
    MATRIX(sub-1,2) = length(Hori);
end



    
