function preproc_wSMI_TMSchans_Alert_Drowsy(params, task_id)
% This function performs processing of wSMI on the HPC, since the wSMI 
% matrix files are too large to be opened on my personal laptop.

% This function takes care of loading the full wSMI files, clustering the
% channels into six anatomically distinct clusters, resulting in a trials x
% timepoints x cluster 3D matrix. 
% Then it indexes this matrix with drowsy and alert trials based on Hori to
% split the data (H1,2 = awake & H3,4,5 = drowsy)

% Define important paths
imp1000folder = '/home/sdv25/rds/hpc-work/Disk_data/Sean/QBI/QBI_Nessa/BVA_downsampling/Import_1000Hz';
datafolder = '/home/sdv25/rds/hpc-work/Datafiles/'; 
resultsfolder = '/home/sdv25/rds/hpc-work/Datafiles/'; 
scriptfolder = '/home/sdv25/neuralkinaware/scripts/';

%% Define channel locations
TMSchans=[6 8 28 30];
nTMSchans=length(TMSchans);
nonTMSchans=[1:63];
nonTMSchans(TMSchans)=[];

%% Load wSMI file and calculate wSMI per cluster comparison
sub = params{1,1};

cd(datafolder);

load(['sub' num2str(sub) '_wSMIwin_tau8ms.mat']);
[nchan1, nchan2, nepochs, ntimes] = size(wsmiwin); 
WSMI_ready=zeros(nTMSchans,nepochs,ntimes); % pre-allocation

% For every epoch
for epo=1:nepochs

    % For every timepoint    
    for tim=1:ntimes
        
        % For every epoch and time window, transpose the lower triangular
        % part so that the zeroes are filled and you get a symmetrical
        % matrix
        wsmi_temp=squeeze(wsmiwin(:,:,epo,tim));
        wsmi_temp=triu(wsmi_temp,0)+triu(wsmi_temp,1).';
        wsmiwin(:,:,epo,tim)=wsmi_temp;
    end
end
    
for c=1:nTMSchans
    wsmiwin_one=squeeze(wsmiwin(TMSchans(c),nonTMSchans,:,:));
    wsmiwin_one_M=squeeze(mean(wsmiwin_one,1));
    WSMI_ready(c,:,:)=wsmiwin_one_M;
end
    
WSMI_ready_M=squeeze(mean(WSMI_ready,1));
    
% baseline correction at single trial level
for epo=1:nepochs
    base=mean(WSMI_ready_M(epo,1:25),2);
    WSMI_ready_M_base(epo,:)=WSMI_ready_M(epo,:)-base;
end

%% Splitting data into alert and drowsy (H1,2 & H3,4,5)

% defining file location
S = [];
S.eeg_filename = ['Sean_sub' num2str(sub) 'epoched_small_epdel_imp_epdel2_imp_base_interp_1000Hz_badchan_trialrejnew_chanrej_ICA1_pruned_interp_filt_ICA2_pruned_interp_interp_base'];
S.eeg_filepath = datafolder;

% initiating eeglab
[ALLEEG EEG CURRENTSET ALLCOM] = eeglab; 
EEG = pop_loadset('filename', [S.eeg_filename '.set'], 'filepath', S.eeg_filepath);
[ALLEEG, EEG, CURRENTSET] = eeg_store( ALLEEG, EEG, 0 );
EEG = eeg_checkset( EEG );
trial_n = EEG.trials;

% Finding indices for each selected condition
ptcpt_resp = [EEG.epoch.RespType] > 0;
ptcpt_hori1 = [EEG.epoch.Hori_stage] == 1;
ptcpt_hori2 = [EEG.epoch.Hori_stage] == 2;
ptcpt_hori3 = [EEG.epoch.Hori_stage] == 3;
ptcpt_hori4 = [EEG.epoch.Hori_stage] == 4;
ptcpt_hori5 = [EEG.epoch.Hori_stage] >= 5;

% Finding trial numbers for each condition
resp_hori1 = find(ptcpt_resp & ptcpt_hori1);
resp_hori2 = find(ptcpt_resp & ptcpt_hori2);
resp_hori3 = find(ptcpt_resp & ptcpt_hori3);
resp_hori4 = find(ptcpt_resp & ptcpt_hori4);
resp_hori5 = find(ptcpt_resp & ptcpt_hori5);

% Merging trial numbers into awake (H1,2) and drowsy (H3,4,5)
resp_hori12 = sort([resp_hori1,resp_hori2]);
resp_hori345 = sort([resp_hori3, resp_hori4, resp_hori5]);

% Saving variables
Awake_trials = resp_hori12;
Drowsy_trials = resp_hori345;

% Index wSMI dataframe to get wSMI values per condition
wsmi_ready_TMSchans_Awake_base = WSMI_ready_M_base(Awake_trials,:,:);
wsmi_ready_TMSchans_Drowsy_base = WSMI_ready_M_base(Drowsy_trials,:,:);
%wsmi_ready_H1_base = wsmi_ready(resp_hori1,:,:);
%wsmi_ready_H2_base = wsmi_ready(resp_hori2,:,:);
%wsmi_ready_H3_base = wsmi_ready(resp_hori3,:,:);
%wsmi_ready_H4_base = wsmi_ready(resp_hori4,:,:);
%wsmi_ready_H5_base = wsmi_ready(resp_hori5,:,:);

% Save variables
save(['sub' num2str(sub) '_wsmi_ready_TMSchans_H12_base.mat'], 'wsmi_ready_TMSchans_Awake_base', '-v7.3');
save(['sub' num2str(sub) '_wsmi_ready_TMSchans_H345_base.mat'], 'wsmi_ready_TMSchans_Drowsy_base', '-v7.3');
save(['sub' num2str(sub) '_wsmi_ready_TMSchans_all.mat'], 'WSMI_ready_M_base', '-v7.3');
%save(['sub' num2str(sub) '_wsmi_ready_H1_base.mat'], 'wsmi_ready_H1_base', '-v7.3');
%save(['sub' num2str(sub) '_wsmi_ready_H2_base.mat'], 'wsmi_ready_H2_base', '-v7.3');
%save(['sub' num2str(sub) '_wsmi_ready_H3_base.mat'], 'wsmi_ready_H3_base', '-v7.3');
%save(['sub' num2str(sub) '_wsmi_ready_H4_base.mat'], 'wsmi_ready_H4_base', '-v7.3');
%save(['sub' num2str(sub) '_wsmi_ready_H5_base.mat'], 'wsmi_ready_H5_base', '-v7.3');
 
end
