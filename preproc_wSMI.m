function preproc_wSMI(params, task_id)
% This function performs processing of wSMI on the HPC, since the wSMI 
% matrix files are too large to be opened on my personal laptop.

% This function takes care of loading the full wSMI files, clustering the
% channels into six anatomically distinct clusters, resulting in a trials x
% timepoints x cluster 3D matrix. 
% Then it indexes this matrix with aware and unaware trials to split the
% data.

% Define important paths
imp1000folder = '/home/sdv25/rds/hpc-work/Disk_data/Sean/QBI/QBI_Nessa/BVA_downsampling/Import_1000Hz';
datafolder = '/home/sdv25/rds/hpc-work/Datafiles/'; 
resultsfolder = '/home/sdv25/rds/hpc-work/Datafiles/'; 
scriptfolder = '/home/sdv25/neuralkinaware/scripts/';

%% Define channel cluster locations
centr_chans = [5 6 7 8 9 10 18 27 28 29 30 31 32 63];
front_chans = [1 2 3 4 19 20 21 22 23 24 25 26 33 34 35 38];
ltemp_chans = [61 36 56 59];
rtemp_chans = [58 57 60 37];
occ_chans = [62 54 53 52 51 50 41 40 39];
pari_chans = [55 49 48 47 46 45 44 43 42 17 16 15 14 13 12 11];
chans_groups = {centr_chans, front_chans, ltemp_chans, rtemp_chans, occ_chans, pari_chans};

%% Load wSMI file and calculate wSMI per cluster comparison
sub = params{1,1};

cd(datafolder);

load(['sub' num2str(sub) '_wSMIwin_tau8ms.mat']);
[nchan1, nchan2, nepochs, ntimes] = size(wsmiwin); 
wsmi_ready=zeros(nepochs,ntimes,30); % pre-allocation for every comparison set: 6 spatial clusters with each other --> 30

% For every epoch
for epo=1:nepochs

    % For every timepoint
    for tim=1:ntimes

        % For every epoch and time window, transpose the lower triangular
        % part so that the zeroes are filled and you get a symmetrical
        % matrix
        wsmi_temp=squeeze(wsmiwin(:,:,epo,tim));
        wsmi_temp=triu(wsmi_temp,0)+triu(wsmi_temp,1).';
        wsmiwin(:,:,epo,tim)=wsmi_temp;
    end   
end

% Create a matrix containing wsmi of trials x timepoints x spatial cluster 
kk = 1;
for i = 1:length(chans_groups)
    for j = 1:length(chans_groups)
        if i == j
            continue
        else
        wsmiwin_one = squeeze(wsmiwin(chans_groups{i},chans_groups{j},:,:));
        wsmiwin_one_M = squeeze(mean(mean(wsmiwin_one,1),2));
        wsmi_ready(:,:,kk) = wsmiwin_one_M;
        kk = kk + 1;
        end
    end
end

%% Splitting data in responsive aware vs. unaware for TMS456

load('trial_matrix.mat');

k = task_id;

% defining file location
S = [];
S.eeg_filename = ['Sean_sub' num2str(sub) 'epoched_small_epdel_imp_epdel2_imp_base_interp_1000Hz_badchan_trialrejnew_chanrej_ICA1_pruned_interp_filt_ICA2_pruned_interp_interp_base'];
S.eeg_filepath = datafolder;

% initiating eeglab
[ALLEEG EEG CURRENTSET ALLCOM] = eeglab; 
EEG = pop_loadset('filename', [S.eeg_filename '.set'], 'filepath', S.eeg_filepath);
[ALLEEG, EEG, CURRENTSET] = eeg_store( ALLEEG, EEG, 0 );
EEG = eeg_checkset( EEG );
epoch_n = EEG.trials;
    
ptcpt_unaware = [EEG.epoch.RespType] == 4;
ptcpt_aware = [EEG.epoch.RespType] == 8;
ptcpt_TMS4 = [EEG.epoch.TMScond] == 4;
ptcpt_TMS5 = [EEG.epoch.TMScond] == 5;
ptcpt_TMS6 = [EEG.epoch.TMScond] == 6;

TMS4_key4 = find(ptcpt_unaware & ptcpt_TMS4);
TMS4_key8 = find(ptcpt_aware & ptcpt_TMS4);
TMS5_key4 = find(ptcpt_unaware & ptcpt_TMS5);
TMS5_key8 = find(ptcpt_aware & ptcpt_TMS5);
TMS6_key4 = find(ptcpt_unaware & ptcpt_TMS6);
TMS6_key8 = find(ptcpt_aware & ptcpt_TMS6);

% TMS4

if trial_matrix(k,1)>trial_matrix(k,2)
    TMS4key4trials=datasample(TMS4_key4,trial_matrix(k,2),'Replace',false);
    TMS4key8trials=TMS4_key8;
elseif trial_matrix(k,1)==trial_matrix(k,2)
    TMS4key4trials=TMS4_key4;
    TMS4key8trials=TMS4_key8;
elseif trial_matrix(k,1)<trial_matrix(k,2)
    TMS4key4trials=TMS4_key4;
    TMS4key8trials=datasample(TMS4_key8,trial_matrix(k,1),'Replace',false);
end

if (trial_matrix(k,1)==0) || (trial_matrix(k,2)==0)
    TMS4key4trials=[];
    TMS4key8trials=[];
end

    
% TMS5

if trial_matrix(k,3)>trial_matrix(k,4)
    TMS5key4trials=datasample(TMS5_key4,trial_matrix(k,4),'Replace',false);
    TMS5key8trials=TMS5_key8;
elseif trial_matrix(k,3)==trial_matrix(k,4)
    TMS5key4trials=TMS5_key4;
    TMS5key8trials=TMS5_key8;
elseif trial_matrix(k,3)<trial_matrix(k,4)
    TMS5key4trials=TMS5_key4;
    TMS5key8trials=datasample(TMS5_key8,trial_matrix(k,3),'Replace',false);
end

if (trial_matrix(k,3)==0) || (trial_matrix(k,4)==0)
TMS5key4trials=[];
TMS5key8trials=[];
end

% TMS6

if trial_matrix(k,5)>trial_matrix(k,6)
    TMS6key4trials=datasample(TMS6_key4,trial_matrix(k,6),'Replace',false);
    TMS6key8trials=TMS6_key8;
elseif trial_matrix(k,5)==trial_matrix(k,6)
    TMS6key4trials=TMS6_key4;
    TMS6key8trials=TMS6_key8;
elseif trial_matrix(k,5)<trial_matrix(k,6)
    TMS6key4trials=TMS6_key4;
    TMS6key8trials=datasample(TMS6_key8,trial_matrix(k,5),'Replace',false);  
end

if (trial_matrix(k,5)==0) || (trial_matrix(k,6)==0)
    TMS6key4trials=[];
    TMS6key8trials=[];
end

% Group aware & unaware trials across TMS intensity
TMS456key4trials=sort([TMS4key4trials,TMS5key4trials,TMS6key4trials]);  
TMS456key8trials=sort([TMS4key8trials,TMS5key8trials,TMS6key8trials]);

% Save variables
aware_trials=TMS456key8trials; % AWARE
unaware_trials=TMS456key4trials; % UNAWARE

% Index wSMI dataframe to get wSMI values per condition
wsmi_ready_T456_aware = wsmi_ready(aware_trials,:,:);
wsmi_ready_T456_unaware = wsmi_ready(unaware_trials,:,:);

% Populate matrix for all participants together - I would need to
% average across trials to be able to do this
% wsmi_ready_T456_aware_all(k,:,:,:) = wsmi_ready_T456_aware(:,:,:);
% wsmi_ready_T456_unaware_all(k,:,:,:) = wsmi_ready_T456_unaware(:,:,:);

% Test code
% wsmi_ready_T456_aware_all_test(1,:,:,:) = wsmi_ready_T456_aware_all;
% wsmi_ready_T456_aware_all_test(2,:,:,:) = wsmi_ready_T456_aware;

% Compute the mean of the first 25 points along the second dimension
base_mat_aware = mean(wsmi_ready_T456_aware(:,1:25,:),2);
base_mat_unaware = mean(wsmi_ready_T456_unaware(:,1:25,:),2);

% Repmat
wsmi_ready_T456_aware_base = wsmi_ready_T456_aware - repmat(base_mat_aware, [1, size(wsmi_ready_T456_aware,2), 1]);
wsmi_ready_T456_unaware_base = wsmi_ready_T456_unaware - repmat(base_mat_unaware, [1, size(wsmi_ready_T456_unaware,2), 1]);

% Save variables
save(['sub' num2str(sub) '_wsmi_ready_T456_aware_base.mat'], 'wsmi_ready_T456_aware_base');
save(['sub' num2str(sub) '_wsmi_ready_T456_unaware_base.mat'], 'wsmi_ready_T456_unaware_base');
save(['sub' num2str(sub) '_wsmi_ready_T456_aware.mat'], 'wsmi_ready_T456_aware');
save(['sub' num2str(sub) '_wsmi_ready_T456_unaware.mat'], 'wsmi_ready_T456_unaware');
save(['sub' num2str(sub) '_wsmi_ready.mat'], 'wsmi_ready');
% wsmi_ready_T456_aware_all_base(k,:,:,:) = wsmi_ready_T456_aware_base;
% wsmi_ready_T456_unaware_all_base(k,:,:,:) = wsmi_ready_T456_unaware_base;

%     WSMI_RT_Awake_M(k)=mean(Awake_RT);
%     WSMI_RT_Drowsy_M(k)=mean(Drowsy_RT);
    
end