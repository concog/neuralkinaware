function preproc_3_test(params)

% Define important paths
imp1000folder = '/home/sdv25/rds/hpc-work/Disk_data/Sean/QBI/QBI_Nessa/BVA_downsampling/Import_1000Hz';
datafolder = '/home/sdv25/rds/hpc-work/Datafiles/'; % Change to Valdas' folder later on;
resultsfolder = '/home/sdv25/rds/hpc-work/Datafiles/';  % perhaps change based on output
scriptfolder = '/home/sdv25/neuralkinaware/scripts/';

%% delete epochs for matching between EEG and MEP data (3)
% For every subject open corresponding data file indicating bad trial numbers 
% and remove them from dataset

sub = params{1,1}; % retrieve the subject's number -- list of subjects available in main() function
    
    [ALLEEG EEG CURRENTSET ALLCOM] = eeglab; 
    % EEG = pop_loadset('filename',['Sean_sub' num2str(sub) 'epoched_small.set'],'filepath', datafolder);
    EEG = pop_loadset('filename',['Sean_sub' num2str(sub) 'epoched_small_epdel_imp.set'],'filepath', 'C:\Datafiles_cam');
    [ALLEEG, EEG, CURRENTSET] = eeg_store( ALLEEG, EEG, 0 );
    EEG = eeg_checkset( EEG );
    
    cd(imp1000folder);
    
    load(['EEG_del_' num2str(sub) '.mat'])
     
    cd(datafolder);
    
    EEG = pop_select( EEG,'notrial',bad_TMS ); % bad_TMS is the name of the dataframe
    [ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 1,'savenew',['Sean_sub' num2str(sub) 'epoched_small_epdel_test.set'],'gui','off'); 
end