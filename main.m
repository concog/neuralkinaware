function main(task_id)

%%MAIN entry point of Matlab SLURM job
%
% Selects file to analyse, loads it, computes LZ of each channel, and save
% the results to CSV. 
%
% Pedro Mediano, Feb 2021

%% Step 0: start up toolboxes
act_tools

%% Step 1: define parameter settings

subjects = {'2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12',...
    '13', '14', '15', '16', '17', '18', '19', '20', '21'};

%% Step 2: fetch task_id from command-line
params = subjects(task_id);

%% Step 3: Run the actual job and save the results
preproc_3_test(params);
