function preproc_4_3_SvM(params)
% This function imports Hori stage information to the dataframes
% It receives a participant number from the
% function main.m 

% Define important paths
imp1000folder = '/home/sdv25/rds/hpc-work/Disk_data/Sean/QBI/QBI_Nessa/BVA_downsampling/Import_1000Hz';
datafolder = '/home/sdv25/rds/hpc-work/Datafiles/'; % Change to Valdas' folder later on;
resultsfolder = '/home/sdv25/rds/hpc-work/Datafiles/';  % perhaps change based on output
scriptfolder = '/home/sdv25/neuralkinaware/scripts/';
valfolder = '/home/sdv25/rds/rds-tb419-bekinschtein/Valdas/QBI/QBI_Nessa/BVA_downsampling/TMS_EEG/';

%% import Hori stage

cd(datafolder);

sub = params{1,1}
    
    [ALLEEG EEG CURRENTSET ALLCOM] = eeglab;
    EEG = pop_loadset('filename',['Sean_sub' num2str(sub) 'epoched_small_epdel_imp_epdel2.set'],'filepath', datafolder);
    [ALLEEG, EEG, CURRENTSET] = eeg_store( ALLEEG, EEG, 0 );
    EEG = eeg_checkset( EEG );
    
    cd(valfolder);
    Hori = xlsread(['QBI_Hori_' num2str(sub)]);

    cd(datafolder);
    trialN = EEG.trials;
    
    for t = 1:trialN
        
        % import Hori stage
        EEG.epoch(t).Hori_stage = Hori(t,1);
                    
    end
  
EEG = eeg_checkset( EEG );
[ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 1,'savenew',['Sean_sub' num2str(sub) 'epoched_small_epdel_imp_epdel2_imp.set'],'gui','off'); 

% save length of trials
trialN_4_3 = EEG.trials;
save([datafolder 'trial_length_2_sub' num2str(sub) '.mat'], 'trialN_4_3');

end


