% Preproc_4_2_SvM
% This part of the script was done on personal computer because it was
% easier and didn't require changing the contents of the main.m script

% Define important paths
clear all
close all
datafolder = 'C:\Datafiles_cam'; % Download MEP and EEG length files to this folder;


%% check how many were deleted
cd(datafolder);

MATRIX = zeros(20,3);

for sub = [2:21]
    
    % before the deletion
    load(['MEP_sub' num2str(sub) '_epdel.mat']);
    MATRIX(sub-1,1) = length(MEP_data_delete);    
    
    % after deletion
    load(['trial_length_sub' num2str(sub) '.mat']);
    MATRIX(sub-1,2) = trialN_4_1;
    
end

MATRIX(:,3) = MATRIX(:,1) - MATRIX(:,2);

