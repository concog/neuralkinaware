function preproc_4_1_SvM(params)
% This function performs epoch deletion for epochs on which pre-trial measures 
% couldn't be calculated reliably. It receives a participant number from the
% function main.m 

% Define important paths
imp1000folder = '/home/sdv25/rds/hpc-work/Disk_data/Sean/QBI/QBI_Nessa/BVA_downsampling/Import_1000Hz';
datafolder = '/home/sdv25/rds/hpc-work/Datafiles/'; % Change to Valdas' folder later on;
resultsfolder = '/home/sdv25/rds/hpc-work/Datafiles/';  % perhaps change based on output
scriptfolder = '/home/sdv25/neuralkinaware/scripts/';

%% delete epochs that were removed from EEG during pre-trial analyses (4)

sub = params{1,1}; % retrieve the subject's number -- list of subjects avaliable in main.m

cd(imp1000folder);
    
load(['Sub_' num2str(sub) '_EEGclean.mat']);

cd(datafolder);

[ALLEEG EEG CURRENTSET ALLCOM] = eeglab; 
EEG = pop_loadset('filename',['Sean_sub' num2str(sub) 'epoched_small_epdel_imp.set'], 'filepath', datafolder);
[ALLEEG, EEG, CURRENTSET] = eeg_store( ALLEEG, EEG, 0 );
EEG = eeg_checkset( EEG );
EEG = pop_select( EEG,'trial',EEGclean );
[ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 1,'savenew',['Sean_sub' num2str(sub) 'epoched_small_epdel_imp_epdel2.set'],'gui','off');

% To circumvent working with a global matrix variable that gets filled up 
% with the difference of EEG trials each loop, here save amount of trials
% and do comparison on my own computer
trialN_4_1 = EEG.trials;
save([datafolder 'trial_length_sub' num2str(sub) '.mat'], 'trialN_4_1');

end
