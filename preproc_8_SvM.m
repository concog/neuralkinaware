function preproc_8_SvM(params)
% This function performs automatic bad channel rejection through a script
% from Sridhar Jagannathan

% This was performed in conjuction with visual bad channel detection and
% differences were individually assessed by me and Valdas. 

% Define important paths
imp1000folder = '/home/sdv25/rds/hpc-work/Disk_data/Sean/QBI/QBI_Nessa/BVA_downsampling/Import_1000Hz';
datafolder = '/home/sdv25/rds/hpc-work/Datafiles/'; % Change to Valdas' folder later on;
resultsfolder = '/home/sdv25/rds/hpc-work/Datafiles/';  % perhaps change based on output
scriptfolder = '/home/sdv25/neuralkinaware/scripts/';

%% Channel rejection

sub = params{1,1};

cd(datafolder);

% 3. automatic detection of bad channels
S = [];
S.eeg_filename = ['Sean_sub' num2str(sub) 'epoched_small_epdel_imp_epdel2_imp_base_interp_1000Hz'];
%S.eeg_filepath = datafolder;
S.eeg_filepath = ['C:\Datafiles_cam\'];
EEG = pop_loadset('filename', [S.eeg_filename '.set'], 'filepath', S.eeg_filepath);
chandata = reshape(EEG.data,EEG.nbchan,EEG.pnts*EEG.trials); % Get chan x tpts..
zerochan = find(var(chandata,0,2) < 0.5); % Remove zero channels from spec..
disp('Zero activity channels were detected: Loading results');
fprintf('--> Channel %d \n',zerochan);
electrodes = setdiff(1:EEG.nbchan,zerochan);
% electrodes = 1:EEG.nbchan;
[~, indelec, ~, ~] = priv_rejchan(EEG,'elec',electrodes ,'threshold',[-3 3],'norm','on',...
                                               'measure','spec' ,'freqrange',[1 45]);
save(['Sean_badelecs_Sri_' num2str(sub)],'indelec');
end
