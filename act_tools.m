%------------------------------ toolboxes ----------------------------%
server = 1 % 1 = server, 0 = laptop

% path definitions

if server == 1
    basefolder = '/home/sdv25/tools/';
    eeglab_path = [basefolder 'eeglab/'];
    ft_path = [basefolder 'fieldtrip/'];
    fica_path = [bsaefolder 'fastICA_25/'];
else
    basefolder = 'C:\Users\seanv\OneDrive - UvA\Study\Master\Cambridge\Project\Analysis\'
    eeglab_path = [basefolder 'eeglab/'];
    ft_path = [basefolder 'fieldtrip/'];
    fica_path = [basefolder 'FastICA_25/'];
end

% fieldtrip set-up
if (exist(ft_path, 'dir') == 7) && ( ~isdeployed)
    addpath(ft_path, '-begin');
    ft_defaults; % find default options and files
    disp('FIELDTRIP IS ALIVE')
elseif ~isdeployed
    disp(['WARNING, CANNOT FIND FIELDTRIP TOOLBOX AT ' ft_path ', CHECK PATHS IN startup.m']);
end

% EEGLAB
if (exist(eeglab_path, 'dir') == 7) && (~isdeployed)
    curdir = pwd;
    cd(eeglab_path);
    eeglab;
    cd(curdir);
    disp('EEGLAB IS ALIVE');
elseif ~isdeployed
    disp(['WARNING, CANNOT FIND EEGLAB TOOLBOX AT ' eeglab_path ', CHECK PATHS IN startup.m']);
end
if ~isdeployed
    % remove conflicting paths
    rmpath(genpath(fullfile(eeglab_path, 'external', 'fieldtrip-partial')));
    % create eeeglabexefolder function to replace eeglab's internal
    % function and put options files in eeglab root
    tmpf = which('eeglabexefolder.m');
    if ~isempty(tmpf)
        if isempty(which('eeglabexefolder_original.m'))
            movefile(tmpf,[ tmpf(1:end-2) '_original.m']);
        end
        fid = fopen(tmpf, 'w');
        fprintf(fid, 'function eeglabdir = eeglabexefolder\n');
        fprintf(fid, '%% This is to replace eeglabs native function, so it knows where to find the option files after compiling.\n');
        fprintf(fid, 'eeglabdir = ''%s'';\n', eeglab_path);
        fclose(fid);
        tmpf = which('eeg_optionsbackup.m');
        copyfile(tmpf, fullfile(eeglabexefolder, 'eeg_optionsbackup.txt')); 
        tmpf = which('eeg_options.m');
        copyfile(tmpf, fullfile(eeglabexefolder, 'eeg_options.txt'));
        disp(['EEGLAB: exefolder is here ' eeglabexefodler]);
    end
end

% fastICA set-up
if (exist(fica_path, 'dir') == 7) && ( ~isdeployed)
    addpath(fica_path, '-begin');
    disp('fastICA IS ALIVE')
elseif ~isdeployed
    disp(['WARNING, CANNOT FIND FICA TOOLBOX AT ' fica_path ', CHECK PATHS IN startup.m']);
end

% add any additional toolboxes here

% clear rubbish
close;
clearvars -except task_id;
% Call Psychtoolbox-3 specific startup function:
if exist('PsychStartup'), PsychStartup; end
