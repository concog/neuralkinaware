function preproc_wSMI_Alert_Drowsy(params, task_id)
% This function performs processing of wSMI on the HPC, since the wSMI 
% matrix files are too large to be opened on my personal laptop.

% This function takes care of loading the full wSMI files, clustering the
% channels into six anatomically distinct clusters, resulting in a trials x
% timepoints x cluster 3D matrix. 
% Then it indexes this matrix with drowsy and alert trials based on Hori to
% split the data (H1,2 = awake & H3,4,5 = drowsy)

% Define important paths
imp1000folder = '/home/sdv25/rds/hpc-work/Disk_data/Sean/QBI/QBI_Nessa/BVA_downsampling/Import_1000Hz';
datafolder = '/home/sdv25/rds/hpc-work/Datafiles/'; 
resultsfolder = '/home/sdv25/rds/hpc-work/Datafiles/'; 
scriptfolder = '/home/sdv25/neuralkinaware/scripts/';

%% Define channel cluster locations
% Old division
% centr_chans = [5 6 7 8 9 10 18 27 28 29 30 31 32 63];
% front_chans = [1 2 3 4 19 20 21 22 23 24 25 26 33 34 35 38];
% ltemp_chans = [61 36 56 59];
% rtemp_chans = [58 57 60 37];
% occ_chans = [62 54 53 52 51 50 41 40 39];
% pari_chans = [55 49 48 47 46 45 44 43 42 17 16 15 14 13 12 11];

% New division
left_frontal = [1 19 21 23];
right_frontal = [2 20 22 24];
left_central = [5 7 9 27 29];
right_central = [6 8 10 28 30];
left_parietal = [11 13 42 44];
right_parietal = [12 14 43 45];
chans_groups = {left_frontal, right_frontal, left_central, right_central, left_parietal, right_parietal};

%% Load wSMI file and calculate wSMI per cluster comparison
sub = params{1,1};

cd(datafolder);

load(['sub' num2str(sub) '_wSMIwin_tau8ms.mat']);
[nchan1, nchan2, nepochs, ntimes] = size(wsmiwin); 
wsmi_ready=zeros(nepochs,ntimes,15); % pre-allocation for every unique comparison set: 6 spatial clusters with each other --> 15

% For every epoch
for epo=1:nepochs

    % For every timepoint
    for tim=1:ntimes

        % For every epoch and time window, transpose the lower triangular
        % part so that the zeroes are filled and you get a symmetrical
        % matrix
        wsmi_temp=squeeze(wsmiwin(:,:,epo,tim));
        wsmi_temp=triu(wsmi_temp,0)+triu(wsmi_temp,1).';
        wsmiwin(:,:,epo,tim)=wsmi_temp;
    end   
end

% Create a matrix containing wsmi of trials x timepoints x spatial cluster 
kk = 1;
for i = 1:length(chans_groups)
    for j = 1:length(chans_groups)
        % Only get unique comparison clusters
        if i >= j
            continue
        else

        % Get wSMI pair values
        wsmiwin_one = squeeze(wsmiwin(chans_groups{i},chans_groups{j},:,:));
        
        % Within comparison pair, average electrode values 
        wsmiwin_one_M = squeeze(mean(mean(wsmiwin_one,1),2));

        % Store averaged trials x timepoints values per comparison pair
        % (3rd dim)
        wsmi_ready(:,:,kk) = wsmiwin_one_M;
        kk = kk + 1;
        end
    end
end

% baseline correction at single trial level
for epo=1:nepochs
    base=mean(wsmi_ready(epo,1:25,:),2);
    wsmi_ready_base(epo,:,:)= wsmi_ready(epo,:,:)-base;
end

%% Splitting data into alert and drowsy (H1,2 & H3,4,5)

% defining file location
S = [];
S.eeg_filename = ['Sean_sub' num2str(sub) 'epoched_small_epdel_imp_epdel2_imp_base_interp_1000Hz_badchan_trialrejnew_chanrej_ICA1_pruned_interp_filt_ICA2_pruned_interp_interp_base'];
S.eeg_filepath = datafolder;

% initiating eeglab
[ALLEEG EEG CURRENTSET ALLCOM] = eeglab; 
EEG = pop_loadset('filename', [S.eeg_filename '.set'], 'filepath', S.eeg_filepath);
[ALLEEG, EEG, CURRENTSET] = eeg_store( ALLEEG, EEG, 0 );
EEG = eeg_checkset( EEG );
trial_n = EEG.trials;

% Finding indices for each selected condition
ptcpt_resp = [EEG.epoch.RespType] > 0;
ptcpt_hori1 = [EEG.epoch.Hori_stage] == 1;
ptcpt_hori2 = [EEG.epoch.Hori_stage] == 2;
ptcpt_hori3 = [EEG.epoch.Hori_stage] == 3;
ptcpt_hori4 = [EEG.epoch.Hori_stage] == 4;
ptcpt_hori5 = [EEG.epoch.Hori_stage] >= 5;

% Finding trial numbers for each condition
resp_hori1 = find(ptcpt_resp & ptcpt_hori1);
resp_hori2 = find(ptcpt_resp & ptcpt_hori2);
resp_hori3 = find(ptcpt_resp & ptcpt_hori3);
resp_hori4 = find(ptcpt_resp & ptcpt_hori4);
resp_hori5 = find(ptcpt_resp & ptcpt_hori5);

% Merging trial numbers into awake (H1,2) and drowsy (H3,4,5)
resp_hori12 = sort([resp_hori1,resp_hori2]);
resp_hori345 = sort([resp_hori3, resp_hori4, resp_hori5]);

% Saving variables
Awake_trials = resp_hori12;
Drowsy_trials = resp_hori345;

% Index wSMI dataframe to get wSMI values per condition
wsmi_ready_Awake_base = wsmi_ready_base(Awake_trials,:,:);
wsmi_ready_Drowsy_base = wsmi_ready_base(Drowsy_trials,:,:);
wsmi_ready_H1_base = wsmi_ready(resp_hori1,:,:);
wsmi_ready_H2_base = wsmi_ready(resp_hori2,:,:);
wsmi_ready_H3_base = wsmi_ready(resp_hori3,:,:);
wsmi_ready_H4_base = wsmi_ready(resp_hori4,:,:);
wsmi_ready_H5_base = wsmi_ready(resp_hori5,:,:);

% Save variables
save(['sub' num2str(sub) '_wsmi_ready_H12_base.mat'], 'wsmi_ready_Awake_base', '-v7.3');
save(['sub' num2str(sub) '_wsmi_ready_H345_base.mat'], 'wsmi_ready_Drowsy_base', '-v7.3');
save(['sub' num2str(sub) '_wsmi_ready_H1_base.mat'], 'wsmi_ready_H1_base', '-v7.3');
save(['sub' num2str(sub) '_wsmi_ready_H2_base.mat'], 'wsmi_ready_H2_base', '-v7.3');
save(['sub' num2str(sub) '_wsmi_ready_H3_base.mat'], 'wsmi_ready_H3_base', '-v7.3');
save(['sub' num2str(sub) '_wsmi_ready_H4_base.mat'], 'wsmi_ready_H4_base', '-v7.3');
save(['sub' num2str(sub) '_wsmi_ready_H5_base.mat'], 'wsmi_ready_H5_base', '-v7.3');
 
end
