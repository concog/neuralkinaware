function preproc_7_SvM(params)
% This function downsamples datafiles from 5kHz to 1kHz.
% It receives a participant number from the function main.m 

% Define important paths
imp1000folder = '/home/sdv25/rds/hpc-work/Disk_data/Sean/QBI/QBI_Nessa/BVA_downsampling/Import_1000Hz';
datafolder = '/home/sdv25/rds/hpc-work/Datafiles/'; % Change to Valdas' folder later on;
resultsfolder = '/home/sdv25/rds/hpc-work/Datafiles/';  % perhaps change based on output
scriptfolder = '/home/sdv25/neuralkinaware/scripts/';

%% downsample to 1000 Hz

sub = params{1,1};

cd(datafolder);
    
    [ALLEEG EEG CURRENTSET ALLCOM] = eeglab;
    EEG = pop_loadset('filename',['Sean_sub' num2str(sub) 'epoched_small_epdel_imp_epdel2_imp_base_interp.set'],'filepath', datafolder);
    [ALLEEG, EEG, CURRENTSET] = eeg_store( ALLEEG, EEG, 0 );
    EEG = eeg_checkset( EEG );

    EEG = pop_resample( EEG, 1000);
    EEG = eeg_checkset( EEG );
    
    EEG = eeg_checkset( EEG );
    [ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 2,'savenew',['Sean_sub' num2str(sub) 'epoched_small_epdel_imp_epdel2_imp_base_interp_1000Hz.set'],'gui','off'); 
  
end

    
  
  
