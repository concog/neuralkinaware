function preproc_3_SvM(params)
% This function performs epoch deletion for epochs without a reliable motor 
% evoked potential. It receives a participant number from the
% function main.m 

% Part of it is commented because I ran this part using MobaXTerm instead
% of job batch submission

% Define important paths
imp1000folder = '/home/sdv25/rds/hpc-work/Disk_data/Sean/QBI/QBI_Nessa/BVA_downsampling/Import_1000Hz';
datafolder = '/home/sdv25/rds/hpc-work/Datafiles/'; % Change to Valdas' folder later on;
resultsfolder = '/home/sdv25/rds/hpc-work/Datafiles/';  % perhaps change based on output
scriptfolder = '/home/sdv25/neuralkinaware/scripts/';

%% delete epochs for matching between EEG and MEP data (3)
% % For every subject open corresponding data file indicating bad trial numbers 
% % and remove them from dataset
% 
% sub = params{1,1}; % retrieve the subject's number -- list of subjects available in main() function
%     
%     [ALLEEG EEG CURRENTSET ALLCOM] = eeglab; 
%     EEG = pop_loadset('filename',['Sean_sub' num2str(sub) 'epoched_small.set'],'filepath','/rds/user/sdv25/hpc-work/Disk_data/Sean/QBI/QBI_Nessa/BVA_downsampling/TMS_EEG/');
%     [ALLEEG, EEG, CURRENTSET] = eeg_store( ALLEEG, EEG, 0 );
%     EEG = eeg_checkset( EEG );
%     
%     cd('/rds/user/sdv25/hpc-work/Disk_data/Sean/QBI/QBI_Nessa/BVA_downsampling/Import_1000Hz/');
%     
%     load(['EEG_del_' num2str(sub) '.mat'])
%      
%     cd('/rds/user/sdv25/hpc-work/Disk_data/Sean/QBI/QBI_Nessa/BVA_downsampling/TMS_EEG/');
%     
%     EEG = pop_select( EEG,'notrial',bad_TMS ); % bad_TMS is the name of the dataframe
%     [ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 1,'savenew',['Sean_sub' num2str(sub) 'epoched_small_epdel.set'],'gui','off'); 
% 
% 
% % check N of deleted epochs
% 
% cd('/rds/user/sdv25/hpc-work/Disk_data/Sean/QBI/QBI_Nessa/BVA_downsampling/Import_1000Hz/');
% 
% MEP_deleted=zeros(20,1);
% 
% for sub = [2:21]
%     
% load(['EEG_del_' num2str(sub) '.mat'])
% MEP_deleted(sub-1)=length(bad_TMS);
% 
% end
% 
% %% check if trial numbers now match between EEG and EMG data files
% 
% MATRIX = zeros(20,2);
% 
% for sub = [2:21]
% 
%     % Load subject files and extract amount of EEG trials
%     [ALLEEG EEG CURRENTSET ALLCOM] = eeglab;
%     EEG = pop_loadset('filename',['Sean_sub' num2str(sub) 'epoched_small_epdel.set'],'filepath','/rds/user/sdv25/hpc-work/Disk_data/Sean/QBI/QBI_Nessa/BVA_downsampling/TMS_EEG/');
%     [ALLEEG, EEG, CURRENTSET] = eeg_store( ALLEEG, EEG, 0 );
%     EEG = eeg_checkset( EEG );
%     
%     MATRIX(sub-1,1)=EEG.trials;
%     
%     cd('/rds/user/sdv25/hpc-work/Disk_data/Sean/QBI/QBI_Nessa/BVA_downsampling/Import_1000Hz/');
%     
%     % Load MEP files and extract amount of trials left after MEP
%     load(['MEP_sub' num2str(sub) '_epdel.mat']);
%     
%     MATRIX(sub-1,2)=length(MEP_data_delete);
% 
% end

%% import ID, trial N, and peak-to-peak amplitude to the structure of each trial

sub = params{1,1}; % retrieve the subject's number -- list of subjects available in main.m
    
    [ALLEEG EEG CURRENTSET ALLCOM] = eeglab;
    EEG = pop_loadset('filename',['Sean_sub' num2str(sub) 'epoched_small_epdel.set'],'filepath', datafolder);
    [ALLEEG, EEG, CURRENTSET] = eeg_store( ALLEEG, EEG, 0 );
    EEG = eeg_checkset( EEG );
  
    cd(imp1000folder);
    
    load(['MEP_sub' num2str(sub) '_epdel.mat']);
    
    trialN = EEG.trials;
    
    for t = 1:trialN
        
        % import ID
        EEG.epoch(t).ID = sub;
        
        % import trial number
        EEG.epoch(t).trialnumber = t;
        
        % import peak to peak amplitude
        EEG.epoch(t).ppamplitude = MEP_data_delete(t,2);
        
        % import TMS condition (not reliable)
        A=0;
        if strcmp(EEG.epoch(t).eventtype{1},'S101')==1
            A=1;
        elseif strcmp(EEG.epoch(t).eventtype{1},'S102')==1
            A=2;
        elseif strcmp(EEG.epoch(t).eventtype{1},'S103')==1
            A=3;
        elseif strcmp(EEG.epoch(t).eventtype{1},'S104')==1
            A=4;
        elseif strcmp(EEG.epoch(t).eventtype{1},'S105')==1
            A=5;
        elseif strcmp(EEG.epoch(t).eventtype{1},'S106')==1
            A=6;
        elseif strcmp(EEG.epoch(t).eventtype{1},'S107')==1
            A=7;
        elseif strcmp(EEG.epoch(t).eventtype{1},'S108')==1
            A=8;
        elseif strcmp(EEG.epoch(t).eventtype{1},'S109')==1
            A=9;
        end

        EEG.epoch(t).TMScond=A;
        clear A
    end
  
EEG = eeg_checkset( EEG );
[ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 1,'savenew',[resultsfolder 'Sean_sub' num2str(sub) 'epoched_small_epdel_imp.set'],'gui','off'); 

end
