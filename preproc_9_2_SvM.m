function preproc_9_2_SvM(params)
% This function performs filtering of the data using the eeglab filtnew
% function. Optimal parameters are automatically calculated by the function

% Define important paths
imp1000folder = '/home/sdv25/rds/hpc-work/Disk_data/Sean/QBI/QBI_Nessa/BVA_downsampling/Import_1000Hz';
datafolder = '/home/sdv25/rds/hpc-work/Datafiles/'; % Change to Valdas' folder later on;
resultsfolder = '/home/sdv25/rds/hpc-work/Datafiles/';  % perhaps change based on output
scriptfolder = '/home/sdv25/neuralkinaware/scripts/';

%% Filtering

sub = params{1,1};

    cd(datafolder);
    
    % definingn file location
    S = [];
    S.eeg_filename = ['Sean_sub' num2str(sub) 'epoched_small_epdel_imp_epdel2_imp_base_interp_1000Hz_badchan_trialrejnew_chanrej_ICA1_pruned_interp'];
    S.eeg_filepath = datafolder;
    
    % initiating eeglab
    [ALLEEG EEG CURRENTSET ALLCOM] = eeglab; 
    EEG = pop_loadset('filename', [S.eeg_filename '.set'], 'filepath', S.eeg_filepath);
    [ALLEEG, EEG, CURRENTSET] = eeg_store( ALLEEG, EEG, 0 );
    EEG = eeg_checkset( EEG );
    
    EEG = pop_eegfiltnew(EEG, 'locutoff', 0.5, 'hicutoff', 45);
    
    % Save dataset
    EEG = eeg_checkset( EEG );
    [ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 1,'savenew',[S.eeg_filepath S.eeg_filename '_filt.set'],'gui','off'); 

end