function preproc_6_SvM(params)
% This function removes the high-amplitude TMS signal in the EEG signal. 
% Times were chosen based on previous inspection of the high-amplitude
% signal, to be found in the NESSA_EEG_preprocessing_TMS script.
% It receives a participant number from the function main.m 


% Define important paths
imp1000folder = '/home/sdv25/rds/hpc-work/Disk_data/Sean/QBI/QBI_Nessa/BVA_downsampling/Import_1000Hz';
datafolder = '/home/sdv25/rds/hpc-work/Datafiles/'; % Change to Valdas' folder later on;
resultsfolder = '/home/sdv25/rds/hpc-work/Datafiles/';  % perhaps change based on output
scriptfolder = '/home/sdv25/neuralkinaware/scripts/';

%% High amplitude removal (6)
% Removes high-amplitude signal and also interpolates this area

% Previously chosen high TMS-artefact times around which we cut
T1=-2; % ms
T2=15; % ms

% Loops through subjects
sub = params{1,1};

cd(datafolder);
    
    % Load datafiles
    [ALLEEG EEG CURRENTSET ALLCOM] = eeglab;
    EEG = pop_loadset('filename',['Sean_sub' num2str(sub) 'epoched_small_epdel_imp_epdel2_imp_base.set'],'filepath', datafolder);
    [ALLEEG, EEG, CURRENTSET] = eeg_store( ALLEEG, EEG, 0 );
    EEG = eeg_checkset( EEG );

    % Find in sample times -2ms to 15ms
    Ac = find(ismember(EEG.times,T1));
    Bc = find(ismember(EEG.times,T2));

    % Put size into variables: r = electrodes (63), e = trials (497, e.g.) c = timepoints (25000)
    [r c e] = size(EEG.data);
    % dataint=zeros(r,c,e); commented out because not used
    
    % Loop through trials
    for i = 1:e

        % Make single-trial dataframes
        Y = squeeze(EEG.data(:,:,i)); % extracts 2D matrices from 3D
        X = EEG.times; % time: 1 x 25000 in my case

        % Loop through electrodes
        % Makes a dataframe Y1 that contains data from all electrodes on
        % timepoints -2ms (column 1) and 15ms (column 2)
        for h = 1:r % extract data points for linear fit
            Y1(h,1) = Y(h,Ac);
            Y1(h,2) = Y(h,Bc);
        end
        
        % Makes a dataframe X1 that contains the datapoint in which
        % TMS-artefact is
        X1(1,1) = X(1,Ac); % extract time points for linear fit
        X1(1,2) = X(1,Bc);

        % Remove artefact in electrode data and timepoints
        Y(:, Ac:Bc) = []; 
        X(:, Ac:Bc) = []; 
        n = size(Y,1); % counts number of rows/electrodes - could have used r variable
        
        % Geeky side-note on polynomial fit: the last number indicates the
        % polynomial degree which you want to fit, so either linear (1),
        % square (2), cube (3), and the corresponding formulas will be a0 +
        % a1x; a0 + a1x + a2x^2; a0 + a1x + a2x^2 + a3x^3. The output of
        % the function will be the a coefficients. So if you give 1 as an
        % input argument, the output will give two coefficients for a (a0 +
        % a1x)
        % Gives 63 x 2 output: in the trial loop evaluates polynomial
        % coefficients that fit best for each electrode. First coefficient
        % is for the a1x part, and second coefficient is for constant (a0)
        for f = 1:n
            P(f,:) = polyfit(X1(1,:),Y1(f,:),1); % fits linear (because last command is 1) function for each channel
        end

        M = EEG.times(:,Ac:Bc); % time points for missing data
        
        % This command does the actual interpolation. Takes the
        % coefficients from the polyfit and calculates data for the
        % specified timeperiod (-2ms to 15ms)
        for g = 1:n
             S(g,:) = polyval(P(g,:),M); % calculates missing data points for each channel
        end
        
        Q = Ac; % sets time point to re-insert data (-2ms --> timesample 4991)
        % This piece of code takes the original datapoint until 1 frame
        % before Ac, then inserts the interpolated part, and then takes the
        % original datapoints from Y (which is dataframe with TMS artefact
        % timepoints deleted) from deletion onwards
        R = [Y(:,1:Q-1) S Y(:,Q:end)]; % inserts linear fitted data into previous artefact area
        EEG.data(:,:,i) = R; % inserts artefact correct matrix into epoch
    end
    
EEG = eeg_checkset( EEG );
[ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 2,'savenew',['Sean_sub' num2str(sub) 'epoched_small_epdel_imp_epdel2_imp_base_interp.set'],'gui','off'); 
  
end