function preproc_10_SvM(params)
% This function performs ICA for large TMS-related muscle artefacts using
% the fastICA algorithm. In accordance with TMS-EEG pipeline from Rogasch
% et al. (2017). It relies on a toolbox called FastICA to which the link
% can be retrieved from Rogasch et al. (2017). 

% This piece of code was used to perform the ICA, store the weights in a
% new dataset, which was subsequently visualised on my personal laptop.

% Define important paths
imp1000folder = '/home/sdv25/rds/hpc-work/Disk_data/Sean/QBI/QBI_Nessa/BVA_downsampling/Import_1000Hz';
datafolder = '/home/sdv25/rds/hpc-work/Datafiles/'; % Change to Valdas' folder later on;
resultsfolder = '/home/sdv25/rds/hpc-work/Datafiles/';  % perhaps change based on output
scriptfolder = '/home/sdv25/neuralkinaware/scripts/';

%% ICA 2
sub = params{1,1};

cd(datafolder);

% definingn file location
S = [];
S.eeg_filename = ['Sean_sub' num2str(sub) 'epoched_small_epdel_imp_epdel2_imp_base_interp_1000Hz_badchan_trialrejnew_chanrej_ICA1_pruned_interp_filt'];
S.eeg_filepath = datafolder;

% initiating eeglab
[ALLEEG EEG CURRENTSET ALLCOM] = eeglab; 
EEG = pop_loadset('filename', [S.eeg_filename '.set'], 'filepath', S.eeg_filepath);
[ALLEEG, EEG, CURRENTSET] = eeg_store( ALLEEG, EEG, 0 );
EEG = eeg_checkset( EEG );

% Replace 0-15 ms with constant amplitude data
EEG = pop_tesa_removedata( EEG, [-2 15] );

% Remove all other artifacts (using FastICA and auto component selection)
EEG = pop_tesa_fastica( EEG, 'approach', 'symm', 'g', 'tanh', 'stabilization', 'off' );

% Save the dataset
EEG = eeg_checkset( EEG );
[ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 1,'savenew',[S.eeg_filepath S.eeg_filename '_ICA2.set'],'gui','off'); 

end