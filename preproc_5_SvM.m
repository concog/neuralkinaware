function preproc_5_SvM(params)
% This function performs baseline correction in -500 to -100 ms. 
% It receives a participant number from the function main.m 

% Define important paths
imp1000folder = '/home/sdv25/rds/hpc-work/Disk_data/Sean/QBI/QBI_Nessa/BVA_downsampling/Import_1000Hz';
datafolder = '/home/sdv25/rds/hpc-work/Datafiles/'; % Change to Valdas' folder later on;
resultsfolder = '/home/sdv25/rds/hpc-work/Datafiles/';  % perhaps change based on output
scriptfolder = '/home/sdv25/neuralkinaware/scripts/';

%% baseline correction (5)

sub = params{1,1}; % retrieve the subject's number -- list of subjects avaliable in main.m

cd(datafolder);
    
    [ALLEEG EEG CURRENTSET ALLCOM] = eeglab;
    EEG = pop_loadset('filename',['Sean_sub' num2str(sub) 'epoched_small_epdel_imp_epdel2_imp.set'],'filepath', datafolder);
    [ALLEEG, EEG, CURRENTSET] = eeg_store( ALLEEG, EEG, 0 );
    EEG = eeg_checkset( EEG );
    
    EEG = pop_rmbase( EEG, [-500 -100]);
    
    EEG = eeg_checkset( EEG );
    [ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 1,'savenew',['Sean_sub' num2str(sub) 'epoched_small_epdel_imp_epdel2_imp_base.set'],'gui','off');
    
end